﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AspTest.Controllers
{
    using AspTest.App_Start;

    using Ninject;

    public class HomeController : Controller
    {
        //
        // GET: /Home/
        [Inject]
        public IWeapon weapon { get; set; }

        public ActionResult Index()
        {
            return View(weapon);
        } 

    }
}
