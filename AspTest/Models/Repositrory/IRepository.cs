﻿using System.Linq;

namespace AspTest.Models.Repositrory
{
    public interface IRepository
    {
        IQueryable<Role> Roles { get; }

        bool CreateRole(Role instance);

        bool UpdateRole(Role instance);

        bool RemoveRole(int idRole); 
    }
}