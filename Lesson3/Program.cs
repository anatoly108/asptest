﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AspTest;

namespace Lesson3
{
    class Program
    {
        static void Main(string[] args)
        {
            var context = new LessonProjectDbDataContext(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

            var roles = context.Roles.ToList();
            foreach (var role in roles)
            {
                Console.WriteLine("{0} {1} {2}", role.Id, role.Code, role.Name);
            }
            Console.ReadLine();

            NewManager(context);
            DeleteManager(context);
            ChangeManagerName(context);
        }

        private static void ChangeManagerName(LessonProjectDbDataContext context)
        {
            var changedRole = context.Roles.FirstOrDefault(p => p.Name == "Менеджер");
            if (changedRole != null)
            {
                changedRole.Name = "Манагер";
                context.Roles.Context.SubmitChanges();
            }
        }

        private static void NewManager(LessonProjectDbDataContext context)
        {
            var newRole = new Role()
            {
                Code = "manager",
                Name = "Менеджер"
            };
            context.Roles.InsertOnSubmit(newRole);
            context.Roles.Context.SubmitChanges();
        }

        private static void DeleteManager(LessonProjectDbDataContext context)
        {
            var deleteRole = context.Roles.FirstOrDefault(p => p.Name == "Менеджер");
            if (deleteRole != null)
            {
                context.Roles.DeleteOnSubmit(deleteRole);
                context.Roles.Context.SubmitChanges();
            }
        }
    }
}
